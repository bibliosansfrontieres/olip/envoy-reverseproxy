# Envoy reverseproxy

This project allows building an [Envoy](https://www.envoyproxy.io/) binary which includes a plugin allowing to inject some HTML code inside
web pages. This plugin has been developped in order to provide a back feature, which adds a button in order to return on the Olip Dashboard.

## Current behavior

This filter works by adding a custom text string by replacing the `</body>` tag in HTML documents.

It detects HTML document by trying to find a `text/html` text string in the content type. In that case, it will perform a string replace on the `</body>` or `</body>` string and appends the script given in parameter.

This system has therefore the following limitations:
* The filter could replace any `</body>` or `</BODY>` string that is not a real HTML tag and have undesirable results. To solve that issue, DOM manipulation (HTML parsing) would be required, but any malformed web page could lead to errors in that case.
* Any tag with an unusual case will not be replaced (i.e. `</BoDy>`)
* HTML web pages that do not conform to the `text/html` mime type will not be handled

## Building

To build the Envoy static binary:

1. `git submodule update --init`
2. `bazel build //:envoy`

Note that depending on the available memory, Bazel may consume too much memory during the compilation. It may be required to limit the number of parallel compilations jobs by appending the `--jobs=n` where `n` is usually the number of CPU cores.

### Building on ARMV7L

The compilation of this software has been tested on armv7l on a
raspberry pi 4 with 4GB of memory. It's however required to
compile bazel from source, as there is no pre-built package available.

The remaining of this guide will suppose that the build is run on a
raspbian buster OS.

First install the following dependencies:

* build-essential
* openjdk-8-jdk
* zip
* unzip
* m4
* automake
* libltdl-dev
* libtool
* libtool-bin
* cmake-data
* libarchive13
* libjsoncpp1
* librhash0
* libuv1
* cmake
* ninja-build

#### Building Bazel

Download the dist package of bazel (see https://github.com/bazelbuild/bazel/releases) and extract it somewhere.

Compiling bazel consist in following this guide: https://docs.bazel.build/versions/2.0.0/install-compile-source.html#bootstrap-bazel by "bootstraping" it. Run the following command in the extracted directory: 
```bash
env EXTRA_BAZEL_ARGS="--host_javabase=@local_jdk//:jdk" BAZEL_JAVAC_OPTS="-J-Xmx2g" bash ./compile.sh
```

Copy the resulting binary (`output/bazel`) in the path.

#### Building envoy

Building envoy requires some additional parameters compared to the usual (amd64) build.

Use the following command instead:

```bash
bazel build //:envoy --cxxopt='-std=gnu++11' --linkopt='-latomic'
```

## Configuring the filter

This filter can be configured using the following configuration block:

```yaml
http_filters: 
  - name: http-injector,
    typed_config:
      @type: type.googleapis.com/olip.Encoder
      script: "<script type='text/javascript' src='http://example.com/my.script.js'></script>"
```

In this example, the `<script>` tag will be append just before the `</body>` tag.